package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "find task by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = IServiceLocator.getAuthService().getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final Task task = IServiceLocator.getTaskService().findOneById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

}
