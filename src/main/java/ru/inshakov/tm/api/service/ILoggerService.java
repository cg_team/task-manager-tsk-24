package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ILoggerService {

    void info (@NotNull String message);

    void debug(@NotNull String message);

    void command (@Nullable String message);

    void error (@NotNull Exception e);

}
