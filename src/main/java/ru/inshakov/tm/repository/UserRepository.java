package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entities.stream().filter(user -> login.equals(user.getLogin())).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return entities.stream().filter(user -> email.equals(user.getEmail())).findFirst().orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        entities.remove(user);
    }

}
